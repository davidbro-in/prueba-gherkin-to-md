# language: es
Característica: Matematica
  Escenario: Hola Mundo
    Dado un archivo llamado "main.py" con:
    """
    print("Hola mundo!")
    """
    Cuando ejecuto `python3 main.py`
    Entonces la salida por consola tiene que ser exactamente "Hola mundo!"

  Esquema del escenario: Sumar numeros
    Dado un archivo llamado "main.py" con una funcion:
    """
    print(<x> + <y>)
    """
    Cuando ejecuto `python3 main.py`
    Entonces la salida por consola tiene que ser exactamente "<z>"
    Ejemplos:
      | x | y | z |
      | 1 | 2 | 3 |
      | 4 | 5 | 9 |
